package ru.kolevatykh.tm.command;

import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.List;
import java.util.Scanner;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    protected final Scanner scanner = new Scanner(System.in);

    public AbstractCommand() {
    }

    public AbstractCommand(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getShortName();

    public abstract String getDescription();

    public abstract boolean needAuth();

    public abstract List<RoleType> isRoleAllowed();

    public abstract void execute() throws Exception;
}
