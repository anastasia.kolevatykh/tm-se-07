package ru.kolevatykh.tm.command.general;

import com.jcabi.manifests.Manifests;
import org.apache.log4j.BasicConfigurator;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class AboutCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getShortName() {
        return "a";
    }

    @Override
    public String getDescription() {
        return "\t\t\tShow build number.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        BasicConfigurator.configure();
        System.out.println("Version: " + Manifests.read("Version"));
        System.out.println("Build number: " + Manifests.read("BuildNumber"));
        System.out.println("Developer: " + Manifests.read("Developer"));
    }
}
