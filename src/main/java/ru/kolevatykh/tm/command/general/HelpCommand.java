package ru.kolevatykh.tm.command.general;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getShortName() {
        return "h";
    }

    @Override
    public String getDescription() {
        return "\t\t\tShow all commands.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}
