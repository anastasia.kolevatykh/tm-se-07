package ru.kolevatykh.tm.command.general;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ExitCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getShortName() {
        return "e";
    }

    @Override
    public String getDescription() {
        return "\t\t\tExit.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[Goodbye!]");
        System.exit(0);
    }
}
