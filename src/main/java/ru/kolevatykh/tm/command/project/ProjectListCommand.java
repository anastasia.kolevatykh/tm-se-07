package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getShortName() {
        return "pls";
    }

    @Override
    public String getDescription() {
        return "\tShow all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");

        String userId = serviceLocator.getUserService().getUser().getId();
        List<Project> projectList = serviceLocator.getProjectService().findAll(userId);

        if (projectList == null) {
            System.out.println("[No projects yet.]");
            return;
        }

        StringBuilder projects = new StringBuilder();
        int i = 0;

        for (Project project : projectList) {
            projects
                    .append(++i)
                    .append(". ")
                    .append(project.toString())
                    .append(System.lineSeparator());
        }

        String projectString = projects.toString();
        System.out.println(projectString);
    }
}
