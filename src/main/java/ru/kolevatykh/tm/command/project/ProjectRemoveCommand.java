package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getShortName() {
        return "pr";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]\nEnter project name: ");
        String name = scanner.nextLine();

        String userId = serviceLocator.getUserService().getUser().getId();

        if (serviceLocator.getProjectService().findOneByName(userId, name) == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
            return;
        }

        String projectId = serviceLocator.getProjectService().findOneByName(userId, name).getId();

        serviceLocator.getTaskService().removeProjectTasks(userId, projectId);
        serviceLocator.getProjectService().remove(userId, projectId);
        System.out.println("[Removed project with tasks.]\n[OK]");
    }
}
