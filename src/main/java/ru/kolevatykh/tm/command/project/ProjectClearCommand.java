package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ProjectClearCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getShortName() {
        return "pcl";
    }

    @Override
    public String getDescription() {
        return "\tRemove all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        String userId = serviceLocator.getUserService().getUser().getId();
        serviceLocator.getTaskService().removeTasksWithProjectId(userId);
        serviceLocator.getProjectService().removeAll(userId);
        System.out.println("[Removed all projects with tasks.]\n[OK]");
    }
}
