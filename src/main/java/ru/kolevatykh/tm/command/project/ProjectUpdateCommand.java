package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectUpdateCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getShortName() {
        return "pu";
    }

    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT UPDATE]\nEnter project name: ");
        String name = scanner.nextLine();

        String userId = serviceLocator.getUserService().getUser().getId();

        if (serviceLocator.getProjectService().findOneByName(userId, name) == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
            return;
        }

        String id = serviceLocator.getProjectService().findOneByName(userId, name).getId();

        System.out.println("Enter new project name: ");
        String nameNew = scanner.nextLine();

        System.out.println("Enter new project description: ");
        String descriptionNew = scanner.nextLine();

        System.out.println("Enter project start date: ");
        String startNew = scanner.nextLine();
        Date startDateNew = DateFormatter.parseDate(startNew);

        System.out.println("Enter project end date: ");
        String endNew = scanner.nextLine();
        Date endDateNew = DateFormatter.parseDate(endNew);

        Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        project.setName(nameNew);
        project.setDescription(descriptionNew);
        project.setStartDate(startDateNew);
        project.setEndDate(endDateNew);

        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }
}
