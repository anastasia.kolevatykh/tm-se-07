package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "project-show";
    }

    @Override
    public String getShortName() {
        return "psh";
    }

    @Override
    public String getDescription() {
        return "\tShow tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT SHOW]\nEnter project name: ");
        String projectName = scanner.nextLine();

        String userId = serviceLocator.getUserService().getUser().getId();

        if (serviceLocator.getProjectService().findOneByName(userId, projectName) == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
            return;
        }

        String projectId = serviceLocator.getProjectService().findOneByName(userId, projectName).getId();

        List<Task> taskList = serviceLocator.getTaskService().findTasksByProjectId(userId, projectId);

        if (taskList != null) {
            System.out.println("[TASK LIST]");

            StringBuilder projectTasks = new StringBuilder();
            int i = 0;

            for (Task task : taskList) {
                if (projectId.equals(task.getProjectId())) {
                    projectTasks
                            .append(++i)
                            .append(". ")
                            .append(task.toString())
                            .append(System.lineSeparator());
                }
            }

            String taskString = projectTasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("[No tasks yet.]");
        }
    }
}
