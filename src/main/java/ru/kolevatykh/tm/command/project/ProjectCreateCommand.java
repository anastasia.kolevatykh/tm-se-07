package ru.kolevatykh.tm.command.project;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectCreateCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getShortName() {
        return "pcr";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        String name = scanner.nextLine();

        System.out.println("Enter project description: ");
        String description = scanner.nextLine();

        System.out.println("Enter project start date: ");
        String projectStartDate = scanner.nextLine();
        Date startDate = DateFormatter.parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        String projectEndDate = scanner.nextLine();
        Date endDate = DateFormatter.parseDate(projectEndDate);

        Project project = new Project(name, description, startDate, endDate);
        project.setUserId(serviceLocator.getUserService().getUser().getId());

        serviceLocator.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}
