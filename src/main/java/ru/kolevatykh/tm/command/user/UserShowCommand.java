package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserShowCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-show";
    }

    @Override
    public String getShortName() {
        return "ush";
    }

    @Override
    public String getDescription() {
        return "\t\tShow all projects and tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER SHOW]");

        User user = serviceLocator.getUserService().getUser();

        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        System.out.println("[ROLE]");
        System.out.println(user.getRoleType().displayName());

        List<Project> projectList = serviceLocator.getProjectService().findAll(user.getId());

        if (projectList == null) {
            System.out.println("[No projects and tasks yet.]");
            return;
        }

        System.out.println("[PROJECT LIST]");
        int projectCount = 0;

        for (Project project : projectList) {
            List<Task> taskList = serviceLocator.getTaskService().findTasksByProjectId(user.getId(), project.getId());

            System.out.println(++projectCount + ". " + project.toString());
            if (taskList != null) {
                System.out.println("[TASK LIST]");

                StringBuilder projectTasks = new StringBuilder();
                int taskCount = 0;

                for (Task task : taskList) {
                        projectTasks
                                .append(++taskCount)
                                .append(". ")
                                .append(task.toString())
                                .append(System.lineSeparator());
                }

                String taskString = projectTasks.toString();
                System.out.println(taskString);
            } else {
                System.out.println("[No tasks yet.]");
            }
        }

        List<Task> taskNotAssignedList = serviceLocator.getTaskService().findTasksWithoutProject(user.getId());

        if (taskNotAssignedList != null) {
            System.out.println("[Not assigned TASKS LIST]");

            StringBuilder taskNotAssigned = new StringBuilder();
            int taskCount = 0;

            for (Task task : taskNotAssignedList) {
                taskNotAssigned
                        .append(++taskCount)
                        .append(". ")
                        .append(task.toString())
                        .append(System.lineSeparator());
            }

            String taskString = taskNotAssigned.toString();
            System.out.println(taskString);
        }
    }
}
