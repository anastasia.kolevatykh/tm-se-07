package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserListCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getShortName() {
        return "uls";
    }

    @Override
    public String getDescription() {
        return "\t\tShow all users.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        List<User> userList = serviceLocator.getUserService().findAll();

        if (userList == null) {
            System.out.println("[No users yet.]");
        }

        StringBuilder users = new StringBuilder();
        int i = 0;

        for (User user : userList) {
            users
                    .append(++i)
                    .append(". ")
                    .append(user.toString())
                    .append(System.lineSeparator());
        }

        String userString = users.toString();
        System.out.println(userString);
    }
}
