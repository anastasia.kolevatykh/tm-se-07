package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserEditCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getShortName() {
        return "ue";
    }

    @Override
    public String getDescription() {
        return "\t\tUpdate login or password.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER EDIT]\nEnter your login for edit: ");
        String login = scanner.nextLine();

        User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist!]");
            return;
        }

        String id = user.getId();

        System.out.println("Enter new login: ");
        String loginNew = scanner.nextLine();

        System.out.println("Enter new password: ");
        String passwordNew = scanner.nextLine();

        user.setLogin(loginNew);
        user.setPasswordHash(passwordNew);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
