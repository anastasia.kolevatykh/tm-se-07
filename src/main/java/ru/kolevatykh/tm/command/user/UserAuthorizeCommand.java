package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.PasswordHash;

import java.util.ArrayList;
import java.util.List;

public final class UserAuthorizeCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-authorize";
    }

    @Override
    public String getShortName() {
        return "ua";
    }

    @Override
    public String getDescription() {
        return "Sign in.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        User curUser = serviceLocator.getUserService().getUser();

        if (curUser != null) {
            System.out.println("[You are authorized under login: " + curUser.getLogin()
                    + "\nPlease LOGOUT first, in order to authorize under OTHER account.]");
            return;
        }

        System.out.println("[USER AUTHORIZATION]\nEnter your login: ");
        String login = scanner.nextLine();

        User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist. Please, retry or register.]");
            return;
        }

        System.out.println("Enter your password: ");
        String password = scanner.nextLine();

        if (!user.getPasswordHash().equals(PasswordHash.getPasswordHash(password))) {
            System.out.println("[Wrong password.]");
            return;
        }

        user.setAuth(true);
        serviceLocator.getUserService().setUser(user);
        System.out.println("[OK]");
    }
}
