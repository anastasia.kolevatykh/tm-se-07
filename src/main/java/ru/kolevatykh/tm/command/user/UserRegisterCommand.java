package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRegisterCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-register";
    }

    @Override
    public String getShortName() {
        return "ur";
    }

    @Override
    public String getDescription() {
        return "\tRegister new user.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        User curUser = serviceLocator.getUserService().getUser();

        if (curUser != null) {
            System.out.println("[You are authorized under login: " + curUser.getLogin()
                    + "\nPlease LOGOUT first, in order to register new account.]");
            return;
        }

        System.out.println("[USER REGISTRATION]\nEnter your login: ");
        String login = scanner.nextLine();

        User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user != null) {
            System.out.println("[Login '" + login + "' already exists. Please, retry.]");
            return;
        }

        System.out.println("Enter your password: ");
        String password = scanner.nextLine();

        user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRoleType(RoleType.USER);
        user.setAuth(true);

        serviceLocator.getUserService().persist(user);
        serviceLocator.getUserService().setUser(user);
        System.out.println("[OK]");
    }
}
