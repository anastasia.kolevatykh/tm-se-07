package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserLogOutCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getShortName() {
        return "ulo";
    }

    @Override
    public String getDescription() {
        return "\tLogout from account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]\nEnter login to confirm logout: ");
        String login = scanner.nextLine();

        User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist!]");
            return;
        }

        String id = serviceLocator.getUserService().getUser().getId();

        System.out.println("Confirm logout, y/n: ");
        String answer = scanner.nextLine();

        if (answer.equals("y")) {
            user.setAuth(false);
            serviceLocator.getUserService().setUser(null);
            System.out.println("[OK]");
        }
    }
}
