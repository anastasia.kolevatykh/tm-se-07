package ru.kolevatykh.tm.command.user;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserDeleteCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "user-delete";
    }

    @Override
    public String getShortName() {
        return "ud";
    }

    @Override
    public String getDescription() {
        return "\tDelete account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER DELETE ACCOUNT]");
        System.out.println("Are you sure you want to delete your account? y/n");
        String answer = scanner.nextLine();

        if (answer.equals("y")) {
            System.out.println("Enter user login: ");
            String login = scanner.nextLine();

            User user = serviceLocator.getUserService().findOneByLogin(login);

            if (user == null) {
                System.out.println("[The user '" + login + "' does not exist!]");
                return;
            }

            String userId = serviceLocator.getUserService().getUser().getId();

            serviceLocator.getTaskService().removeAll(userId);
            serviceLocator.getProjectService().removeAll(userId);
            serviceLocator.getUserService().remove(userId);
            serviceLocator.getUserService().setUser(null);
            System.out.println("[Deleted user account with projects and tasks.]\n[OK]");
        }
    }
}
