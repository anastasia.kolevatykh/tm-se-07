package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getShortName() {
        return "tr";
    }

    @Override
    public String getDescription() {
        return "\tRemove selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]\nEnter task name: ");
        String taskName = scanner.nextLine();

        String userId = serviceLocator.getUserService().getUser().getId();

        if (serviceLocator.getTaskService().findOneByName(userId, taskName) == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
            return;
        }

        String taskId = serviceLocator.getTaskService().findOneByName(userId, taskName).getId();

        serviceLocator.getTaskService().remove(userId, taskId);
        System.out.println("[OK]");
    }
}
