package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskUpdateCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getShortName() {
        return "tu";
    }

    @Override
    public String getDescription() {
        return "\tUpdate selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[TASK UPDATE]\nEnter task name: ");
        String name = scanner.nextLine();

        String userId = serviceLocator.getUserService().getUser().getId();

        if (serviceLocator.getTaskService().findOneByName(userId, name) == null) {
            System.out.println("[The task '" + name + "' does not exist!]");
            return;
        }

        String id = serviceLocator.getTaskService().findOneByName(userId, name).getId();

        System.out.println("Enter new task name: ");
        String nameNew = scanner.nextLine();

        System.out.println("Enter new task description: ");
        String descriptionNew = scanner.nextLine();

        System.out.println("Enter new task start date: ");
        String startNew = scanner.nextLine();
        Date startDateNew = DateFormatter.parseDate(startNew);

        System.out.println("Enter new task end date: ");
        String endNew = scanner.nextLine();
        Date endDateNew = DateFormatter.parseDate(endNew);

        Task task = serviceLocator.getTaskService().findOneByName(userId, name);
        task.setName(nameNew);
        task.setDescription(descriptionNew);
        task.setStartDate(startDateNew);
        task.setEndDate(endDateNew);

        serviceLocator.getTaskService().merge(task);
        System.out.println("[OK]");
    }
}
