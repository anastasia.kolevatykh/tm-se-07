package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class TaskAssignCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "task-assign";
    }

    @Override
    public String getShortName() {
        return "ta";
    }

    @Override
    public String getDescription() {
        return "\tAssign task to project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[ASSIGN TASK TO PROJECT]\nEnter task name:");
        String taskName = scanner.nextLine();

        String userId = serviceLocator.getUserService().getUser().getId();

        if (serviceLocator.getTaskService().findOneByName(userId, taskName) == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
            return;
        }

        String taskId = serviceLocator.getTaskService().findOneByName(userId, taskName).getId();

        System.out.println("Enter project name:");
        String projectName = scanner.nextLine();

        if (serviceLocator.getProjectService().findOneByName(userId, projectName) == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
            return;
        }

        String projectId = serviceLocator.getProjectService().findOneByName(userId, projectName).getId();

        serviceLocator.getTaskService().assignToProject(userId, taskId, projectId);
        System.out.println("[OK]");
    }
}
