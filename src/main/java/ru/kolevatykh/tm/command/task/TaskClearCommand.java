package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class TaskClearCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getShortName() {
        return "tcl";
    }

    @Override
    public String getDescription() {
        return "\tRemove all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        String userId = serviceLocator.getUserService().getUser().getId();
        serviceLocator.getTaskService().removeAll(userId);
        System.out.println("[Removed all tasks.]");
        System.out.println("[OK]");
    }
}
