package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskCreateCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getShortName() {
        return "tcr";
    }

    @Override
    public String getDescription() {
        return "\tCreate new task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\nEnter task name: ");
        String name = scanner.nextLine();

        System.out.println("Enter task description: ");
        String description = scanner.nextLine();

        System.out.println("Enter task start date: ");
        String taskStartDate = scanner.nextLine();
        Date startDate = DateFormatter.parseDate(taskStartDate);

        System.out.println("Enter task end date: ");
        String taskEndDate = scanner.nextLine();
        Date endDate = DateFormatter.parseDate(taskEndDate);

        Task task = new Task(name, description, startDate, endDate);
        task.setUserId(serviceLocator.getUserService().getUser().getId());

        serviceLocator.getTaskService().persist(task);
        System.out.println("[OK]");
    }
}
