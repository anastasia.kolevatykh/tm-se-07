package ru.kolevatykh.tm.command.task;

import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getShortName() {
        return "tls";
    }

    @Override
    public String getDescription() {
        return "\t\tShow all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");

        String userId = serviceLocator.getUserService().getUser().getId();
        List<Task> taskList = serviceLocator.getTaskService().findAll(userId);

        if (taskList == null) {
            System.out.println("[No tasks yet.]");
            return;
        }

        StringBuilder tasks = new StringBuilder();
        int i = 0;

        for (Task task : taskList) {
            tasks
                    .append(++i)
                    .append(". ")
                    .append(task.toString())
                    .append(System.lineSeparator());
        }

        String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
