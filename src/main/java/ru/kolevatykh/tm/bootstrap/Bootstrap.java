package ru.kolevatykh.tm.bootstrap;

import ru.kolevatykh.tm.api.*;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.repository.*;
import ru.kolevatykh.tm.service.*;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final Scanner scanner = new Scanner(System.in);

    private final IUserRepository userRepository = new UserRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    private void registryCommand(final AbstractCommand command) throws Exception {
        final String commandName = command.getName();
        final String commandShortName = command.getShortName();
        final String commandDescription = command.getDescription();

        if (commandName == null || commandName.isEmpty()) {
            throw new Exception("There's no such command name.");
        }
        if (commandDescription == null || commandDescription.isEmpty()) {
            throw new Exception("There's no such command description.");
        }
        command.setServiceLocator(this);
        commands.put(commandName, command);

        if (commandShortName != null && !commandShortName.isEmpty()) {
            commands.put(commandShortName, command);
        }
    }

    private void start() throws Exception {
        System.out.println("*** Welcome to task manager ***"
                + "\nType \"help\" for details.");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        if (abstractCommand.needAuth() && userService.getUser() == null) {
            throw new Exception("[Command is not allowed. You need to authorize or register.]");
        }
        if (abstractCommand.isRoleAllowed().contains(RoleType.ADMIN)
                && !abstractCommand.isRoleAllowed().contains(RoleType.USER)
                && !userService.getUser().getRoleType().equals(RoleType.ADMIN)) {
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        }
        abstractCommand.execute();
    }

    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init(final AbstractCommand... commandInstances) {
        try {
            for (final AbstractCommand command : commandInstances) {
                registryCommand(command);
            }
            userService.createTestUsers();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
