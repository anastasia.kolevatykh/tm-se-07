package ru.kolevatykh.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateFormatter {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    public static Date parseDate(final String dateInput) {
        try {
            return DATE_FORMAT.parse(dateInput);
        } catch (ParseException pe) {
            System.out.println("Parse date exception: Wrong date format, for instance 30.1.2020.");
            return null;
        }
    }
}
