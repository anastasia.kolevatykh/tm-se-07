package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.enumerate.RoleType;

public final class UserService extends AbstractService<User> implements IUserService {
    private IUserRepository userRepository;
    private User currentUser;

    public UserService() {

    }

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void setUser(final User user) {
        this.currentUser = user;
    }

    @Override
    public User getUser() {
        return currentUser;
    }

    @Override
    public User findOneByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findOneByLogin(login);
    }

    public void createTestUsers() {
        User admin = new User("admin", "pass1", RoleType.ADMIN);
        User user = new User("user", "pass2", RoleType.USER);
        userRepository.persist(admin);
        userRepository.persist(user);
    }
}
