package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;
import ru.kolevatykh.tm.api.IProjectTaskRepository;

import java.util.List;

public abstract class AbstractProjectTaskService<T extends AbstractProjectTaskEntity> {
    private IProjectTaskRepository<T> projectTaskRepository;

    AbstractProjectTaskService() {

    }

    AbstractProjectTaskService(final IProjectTaskRepository<T> projectTaskRepository) {
        this.projectTaskRepository = projectTaskRepository;
    }

    public List<T> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<T> taskList = projectTaskRepository.findAll(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    public T findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return null;
        return projectTaskRepository.findOneById(userId, id);
    }

    public T findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        return projectTaskRepository.findOneByName(userId, name);
    }

    public void persist(final T entity) {
        if (entity == null) return;
        projectTaskRepository.persist(entity);
    }

    public void merge(final T entity) {
        if (entity == null) return;
        projectTaskRepository.merge(entity);
    }

    public void remove(final String userId, final String id) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return;
        projectTaskRepository.remove(userId, id);
    }

    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectTaskRepository.removeAll(userId);
    }
}
