package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.api.IRepository;
import ru.kolevatykh.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> {
    private IRepository<T> repository;

    AbstractService() {

    }

    AbstractService(final IRepository<T> repository) {
        this.repository = repository;
    }

    public List<T> findAll() {
        return repository.findAll();
    }

    public T findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOneById(id);
    }

    public void persist(final T entity) {
        if (entity == null) return;
        repository.persist(entity);
    }

    public void merge(final T entity) {
        if (entity == null) return;
        repository.merge(entity);
    }

    public void remove(final String id) {
        if (id == null || id.isEmpty()) return;
        repository.remove(id);
    }

    public void removeAll() {
        repository.removeAll();
    }
}
