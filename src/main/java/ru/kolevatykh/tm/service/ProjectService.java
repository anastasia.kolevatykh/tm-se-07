package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.api.IProjectService;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.api.IProjectRepository;

public final class ProjectService extends AbstractProjectTaskService<Project> implements IProjectService {
    private IProjectRepository projectRepository;


    public ProjectService() {

    }

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }
}
