package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.api.ITaskService;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.api.ITaskRepository;

import java.util.List;

public final class TaskService extends AbstractProjectTaskService<Task> implements ITaskService {
    private ITaskRepository taskRepository;

    public TaskService() {

    }

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void removeTasksWithProjectId(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeTasksWithProjectId(userId);
    }

    @Override
    public void removeProjectTasks(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty() || userId == null || userId.isEmpty()) return;
        taskRepository.removeProjectTasks(userId, projectId);
    }

    @Override
    public List<Task> findTasksByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty() || userId == null || userId.isEmpty()) return null;
        List<Task> taskList = taskRepository.findTasksByProjectId(userId, projectId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Override
    public List<Task> findTasksWithoutProject(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Task> taskList = taskRepository.findTasksWithoutProject(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Override
    public void assignToProject(final String userId, final String id, final String projectId) {
        if (id == null || id.isEmpty() || projectId == null || projectId.isEmpty()
                || userId == null || userId.isEmpty()) return;
        taskRepository.assignToProject(userId, id, projectId);
    }
}
