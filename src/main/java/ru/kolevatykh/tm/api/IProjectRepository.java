package ru.kolevatykh.tm.api;

import ru.kolevatykh.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IProjectTaskRepository<Project> {
    List<Project> findAll(String userId);

    Project findOneById(String userId, String id);

    Project findOneByName(String userId, String name);

    void persist(Project project);

    void merge(Project project);

    void remove(String userId, String id);

    void removeAll(String userId);
}
