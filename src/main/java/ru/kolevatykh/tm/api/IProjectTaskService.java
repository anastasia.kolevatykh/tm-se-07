package ru.kolevatykh.tm.api;

import java.util.List;

public interface IProjectTaskService<T> {
    List<T> findAll(String userId);

    T findOneById(String userId, String id);

    T findOneByName(String userId, String name);

    void persist(T entity);

    void merge(T entity);

    void remove(String userId, String id);

    void removeAll(String userId);
}
