package ru.kolevatykh.tm.api;

import ru.kolevatykh.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IProjectTaskRepository<Task> {
    List<Task> findAll(String userId);

    Task findOneById(String userId, String id);

    Task findOneByName(String userId, String name);

    void persist(Task task);

    void merge(Task task);

    void remove(String userId, String id);

    void removeAll(String userId);

    void removeTasksWithProjectId(String userId);

    void removeProjectTasks(String userId, String projectId);

    List<Task> findTasksByProjectId(String userId, String projectId);

    List<Task> findTasksWithoutProject(final String userId);

    void assignToProject(String userId, String id, String projectId);
}
