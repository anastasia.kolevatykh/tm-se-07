package ru.kolevatykh.tm.api;

import java.util.List;

public interface IService<T> {
    List<T> findAll();

    T findOneById(String id);

    void persist(T entity);

    void merge(T entity);

    void remove(String id);

    void removeAll();
}
