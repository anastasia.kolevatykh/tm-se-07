package ru.kolevatykh.tm.api;

import ru.kolevatykh.tm.command.AbstractCommand;

import java.util.Collection;

public interface ServiceLocator {
    IUserService getUserService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    Collection<AbstractCommand> getCommands();
}
