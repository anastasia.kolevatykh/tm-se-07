package ru.kolevatykh.tm.api;

import ru.kolevatykh.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {
    void setUser(final User user);

    User getUser();

    List<User> findAll();

    User findOneById(String id);

    User findOneByLogin(String login);

    void persist(User user);

    void merge(User user);

    void remove(String id);

    void removeAll();

    void createTestUsers();
}
