package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;

import java.util.*;

public abstract class AbstractProjectTaskRepository<T extends AbstractProjectTaskEntity> {
    protected final Map<String, T> map = new HashMap<>();

    public List<T> findAll(final String userId) {
        List<T> taskList = new ArrayList<>();
        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    public T findOneById(final String userId, final String id) {
        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getId().equals(id))
                return entry.getValue();
        }
        return null;
    }

    public T findOneByName(final String userId, final String name) {
        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getName().equals(name))
                return entry.getValue();
        }
        return null;
    }

    public void merge(final T entity) {
        map.put(entity.getId(), entity);
    }

    public void remove(final String userId, final String id) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            T entity = entry.getValue();
            if (entity.getUserId().equals(userId)
                    && entity.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll(final String userId) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            if (entry.getValue().getUserId().equals(userId))
                it.remove();
        }
    }
}
