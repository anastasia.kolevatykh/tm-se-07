package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.entity.Project;

public final class ProjectRepository extends AbstractProjectTaskRepository<Project> implements IProjectRepository {

    @Override
    public void persist(final Project project) {
        map.put(project.getId(), project);
    }
}
