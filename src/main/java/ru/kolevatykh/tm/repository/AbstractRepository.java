package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractEntity> {
    protected final Map<String, T> map = new HashMap<>();

    public List<T> findAll() {
        final List<T> users = new ArrayList<>(map.values());
        return users;
    }

    public T findOneById(final String id) {
        return map.get(id);
    }

    public void persist(final T user) {
        map.put(user.getId(), user);
    }

    public void merge(final T user) {
        map.put(user.getId(), user);
    }

    public void remove(final String id) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            T task = entry.getValue();
            if (task.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll() {
        map.clear();
    }
}
