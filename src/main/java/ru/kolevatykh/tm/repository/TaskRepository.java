package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.api.ITaskRepository;
import ru.kolevatykh.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractProjectTaskRepository<Task> implements ITaskRepository {

    @Override
    public void persist(final Task task) {
        map.put(task.getId(), task);
    }

    @Override
    public void removeTasksWithProjectId(String userId) {
        for (Iterator<Map.Entry<String, Task>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getUserId().equals(userId)
                    && task.getProjectId() != null)
                it.remove();
        }
    }

    @Override
    public void removeProjectTasks(final String userId, final String projectId) {
        for (Iterator<Map.Entry<String, Task>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getUserId().equals(userId)
                    && task.getProjectId().equals(projectId))
                it.remove();
        }
    }

    @Override
    public List<Task> findTasksByProjectId(final String userId, final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : map.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && projectId.equals(entry.getValue().getProjectId()))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @Override
    public List<Task> findTasksWithoutProject(final String userId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : map.entrySet()) {
            if (entry.getValue().getUserId().equals(userId)
                    && entry.getValue().getProjectId() == null)
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @Override
    public void assignToProject(final String userId, final String id, final String projectId) {
        if (map.get(id).getUserId().equals(userId))
            map.get(id).setProjectId(projectId);
    }
}
