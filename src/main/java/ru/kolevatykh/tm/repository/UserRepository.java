package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.entity.User;

import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        for (Map.Entry<String, User> entry : map.entrySet()) {
            if (entry.getValue().getLogin().equals(login))
                return entry.getValue();
        }
        return null;
    }
}
