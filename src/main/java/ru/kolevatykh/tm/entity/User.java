package ru.kolevatykh.tm.entity;

import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.PasswordHash;

public class User extends AbstractEntity {
    private String login;
    private String passwordHash;
    private RoleType roleType;
    private boolean auth = false;

    public User() {
        super();
    }

    public User(final String login, final String password, final RoleType roleType) {
        this();
        this.login = login;
        this.passwordHash = PasswordHash.getPasswordHash(password);
        this.roleType = roleType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final String password) {
        this.passwordHash = PasswordHash.getPasswordHash(password);
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(final RoleType roleType) {
        this.roleType = roleType;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(final boolean auth) {
        this.auth = auth;
    }

    @Override
    public String toString() {
        return "id: '" + getId() + '\'' +
                ", login: '" + login + '\'' +
                ", roleType: " + roleType +
                ", isAuth: " + isAuth();
    }
}
