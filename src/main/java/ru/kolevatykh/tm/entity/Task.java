package ru.kolevatykh.tm.entity;

import java.util.Date;

public class Task extends AbstractProjectTaskEntity {
    private String projectId;

    public Task() {
        super();
    }

    public Task(final String name, final String description, final Date startDate, final Date endDate) {
        super(name, description, startDate, endDate);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "user id: '" + getUserId() + '\'' +
                ", project id: '" + projectId + '\'' +
                ", id: '" + getId() + '\'' +
                ", name: '" + getName() + '\'' +
                ", description: '" + getDescription() + '\'' +
                ", startDate: " + getStartDate() +
                ", endDate: " + getEndDate();
    }
}
