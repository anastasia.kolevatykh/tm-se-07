package ru.kolevatykh.tm.entity;

import java.util.Date;

public class Project extends AbstractProjectTaskEntity {

    public Project() {
        super();
    }

    public Project(final String name, final String description, final Date startDate, final Date endDate) {
        super(name, description, startDate, endDate);
    }

    @Override
    public String toString() {
        return "user id: '" + getUserId() + '\'' +
                ", id: '" + getId() + '\'' +
                ", name: '" + getName() + '\'' +
                ", description: '" + getDescription() + '\'' +
                ", startDate: " + getStartDate() +
                ", endDate: " + getEndDate();
    }
}
