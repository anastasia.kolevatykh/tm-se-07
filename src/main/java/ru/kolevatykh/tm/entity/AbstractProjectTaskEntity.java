package ru.kolevatykh.tm.entity;

import java.util.Date;

public abstract class AbstractProjectTaskEntity extends AbstractEntity {
    private String userId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;

    AbstractProjectTaskEntity() {
        super();
    }

    AbstractProjectTaskEntity(final String name, final String description, final Date startDate, final Date endDate) {
        this();
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }
}
